# arisu's zsh config


# Oh my zsh
export ZSH="$HOME/.oh-my-zsh"
plugins=(git)
COMPLETION_WAITING_DOTS="true"
ZSH_THEME="powerlevel10k/powerlevel10k" # `p10k configure`
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
source $ZSH/oh-my-zsh.sh


# LANG
export LANG=en_US.UTF-8


# Auto completion
fpath=(~/.zsh/functions $fpath)
autoload -U compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' special-dirs false
zmodload zsh/complist
compinit
_comp_options+=(globdots)


# PATH
export PATH="$PATH:$(ruby -e 'puts Gem.user_dir')/bin" # Ruby
export PATH="$PATH:$HOME/.composer/vendor/bin" # Composer
export PATH="$HOME/dev/android/platform-tools:$HOME/dev/android/bin:$PATH" # Android platform tools
export PATH="$PATH:$HOME/.rvm/bin" # Ruby rvm


# Editor
export EDITOR=micro
VISUAL=$EDITOR


## Aliases ##
setopt complete_aliases

alias dotgit='git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
compdef dotgit='git'
alias authy="/usr/bin/electron4 --app /usr/lib/authy/app.asar"

alias syslog="sudo journalctl -f | ccze -A"
journal() {
	sudo journalctl -f -u $1 | ccze -A
}

alias nano=micro

# Random chars
rndanchars() {
	openssl rand -base64 $1 | sed ':a;N;$!ba;s/\n//g' | sed 's/[+\/=]//g' | head -c $1
}

# FFMPEG
convert-tomov() {
	ffmpeg -i "$1" -c:v dnxhd -c:a pcm_s24le -profile:v dnxhr_hq "$2"
}

convert-tomp4() {
	ffmpeg -i "$1" -c:v libx264 -pix_fmt yuv420p -profile:v high -movflags faststart -c:a aac -ar 48000 -b:a 384000 "$2"
}

convert-todavinci() {
	ffmpeg -i "$1" -map 0:v -map "0:a?" -c:v mpeg4 -qscale:v 1 -c:a pcm_s24le "$1.davinci.mov"
}


# Syntax highlighting (should be last)
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
