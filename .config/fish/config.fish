#set -x XDG_CURRENT_DESKTOP=kde

if test ! $DISPLAY && test $XDG_VTNR -eq 1
  exec startx
end


# LANG
set -x LANG en_US.UTF-8


# PATH
set -x PATH "$PATH:"(ruby -e 'puts Gem.user_dir')"/bin" # Ruby
set -x PATH "$PATH:$HOME/.composer/vendor/bin" # Composer
set -x PATH "$HOME/dev/android/platform-tools:$HOME/dev/android/bin:$PATH" # Android platform tools
set -x PATH "$PATH:$HOME/.rvm/bin" # Ruby rvm


# Editor
set -x EDITOR micro
set -x VISUAL $EDITOR


## Aliases ##
alias l='ls -la'
alias dotgit='git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
alias authy="/usr/bin/electron4 --app /usr/lib/authy/app.asar"

alias syslog="sudo journalctl -f | ccze -A"
function journal -w 'systemctl status'
	journalctl -f -u $argv | ccze -A
end

alias nano=micro

# Random chars
function rndanchars
	openssl rand -base64 $1 | sed ':a;N;$!ba;s/\n//g' | sed 's/[+\/=]//g' | head -c $1
end

# FFMPEG
function convert-tomov
	ffmpeg -i "$1" -c:v dnxhd -c:a pcm_s24le -profile:v dnxhr_hq "$2"
end

function convert-tomp4
	ffmpeg -i "$1" -q:v 1 -q:a 1 -c:v libx264 -pix_fmt yuv420p -profile:v high -movflags faststart -c:a aac -ar 48000 -b:a 384000 "$2"
end

function convert-todavinci
	set name "$1"
	shift
	
	ffmpeg -i "$name" "$argv" -q:v 1 -q:a 1 -map 0 -c:v mpeg4 -c:a pcm_s24le "$name.davinci.mov"
end

# Example: `pngs-to-mov "%04d" 60 render` renders images with format 0000.png at 60 fps to render.mov
function pngs-to-mov
	ffmpeg -r $2 -i "$1.png" -c:v qtrle "$3.mov"
end

function convert-toformat-simple
	ffmpeg -i "$1" -q:v 1 -q:a 0 -map 0 -c:v libx265 -c:a copy -c:s copy "$2"
end
