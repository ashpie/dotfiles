#export XDG_CURRENT_DESKTOP=kde

if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx
fi
